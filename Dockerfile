FROM public.ecr.aws/lambda/python:3.8

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1
# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

# AWS CREDENTIALS
ENV bucket_name="loom-pptx"
ENV mongo_user="server"
ENV mongo_pass="server2020"

# Install pip requirements
COPY requirements.txt ${LAMBDA_TASK_ROOT}
RUN python3.8 -m pip install --upgrade pip && python3.8 -m pip install -r requirements.txt

COPY src/ ${LAMBDA_TASK_ROOT}

# Set the CMD to your handler (could also be done as a parameter override outside of the Dockerfile)
CMD [ "lambda_function.lambda_handler" ]