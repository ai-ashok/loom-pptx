from pptx import Presentation
from pptx.util import Pt


def txt2ppt(qas, id, savepath):
    prs = Presentation()
    title_slide_layout = prs.slide_layouts[1]

    for qa in qas:
        slide = prs.slides.add_slide(title_slide_layout)
        title = slide.shapes.title
        title.text = qa["question"]

        body_shape = slide.placeholders[1]
        tf = body_shape.text_frame
        pg = tf.add_paragraph()
        pg.text = qa["answer"]
        pg.font.size = Pt(20)

    prs.save(savepath)
