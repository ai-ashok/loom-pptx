import os
import boto3

BUCKET_NAME = os.environ.get("bucket_name")

S3_CLIENT = boto3.client("s3")


def upload_to_s3(local_path, s3_path):
    print(f"Uploading: {local_path} to s3://loom-pptx/{s3_path}")
    S3_CLIENT.upload_file(local_path, BUCKET_NAME, s3_path)
    print(f"Uploaded: {local_path} to s3://loom-pptx/{s3_path}")
