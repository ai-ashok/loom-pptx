from app import get_ppt

def lambda_handler(event, context):
    id = event["id"]
    try:
        collection = event["collections"]
    except:
        collection = "prezoomanswers"

    response = get_ppt(id, collection)
    return response