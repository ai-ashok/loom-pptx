import pymongo
import os

mongo_user = os.environ["mongo_user"]
mongo_pass = os.environ["mongo_pass"]

client = pymongo.MongoClient(
    f"mongodb+srv://{mongo_user}:{mongo_pass}@videosummary.46r2t.mongodb.net/"
)
db = client["<dbname>"]


def get_mongo_data(collection_name, id):
    collection = db[collection_name]
    document = collection.find_one({"videoId": id})
    return document


def update_mongo(collection_name, id, s3URI, objectURL):
    collection = db[collection_name]
    collection.update_one(
        {"videoId": id}, {"$set": {"s3URI": s3URI, "objectURL": objectURL}}
    )
