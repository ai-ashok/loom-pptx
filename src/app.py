from mongo import get_mongo_data, update_mongo
from txt2ppt import txt2ppt
from s3 import upload_to_s3


def get_ppt(id, collection):
    try:
        filename = f"loom_{id}.pptx"
        save_path = f"/tmp/{filename}"
        document = get_mongo_data(collection, id)
        qas = document["answers"]
        txt2ppt(qas, id, savepath=save_path)
        upload_to_s3(save_path, filename)
        s3URI = f"s3://loom-pptx/{filename}"
        objectURL = f"https://loom-pptx.s3.ap-south-1.amazonaws.com/{filename}"
        update_mongo(collection, id, s3URI, objectURL)
        return {"statusCode": 200, "s3URI": s3URI, "objectURL": objectURL}
    except Exception as err:
        return {"statusCode": 500, "message": f"error: {err}"}
